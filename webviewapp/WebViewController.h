//
//  WebViewController.h
//  webviewapp
//
//  Created by Arsy on 11/6/12.
//  Copyright (c) 2012 Arsy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
{
    BOOL _loaded;
}

@property (nonatomic, retain) NSString* targetUrl;
@property (nonatomic, retain) IBOutlet UIWebView* webView;
@property (nonatomic, retain) UIActivityIndicatorView* activityIndicator;

- (id)initWithWebPage:(NSString*)url withTitle:(NSString*)title withIcon:(NSString*)iconName;

@end
