//
//  WebViewController.m
//  webviewapp
//
//  Created by Arsy on 11/6/12.
//  Copyright (c) 2012 Arsy. All rights reserved.
//

#import "WebViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize targetUrl;
@synthesize webView;
@synthesize activityIndicator;


- (id)initWithWebPage:(NSString*)url withTitle:(NSString*)title withIcon:(NSString*)iconName
{
    self = [super initWithNibName:@"WebViewController" bundle:nil];
    
    if (self)
    {
        self.targetUrl = url;
        self.title = title;
        self.tabBarItem.image = [UIImage imageNamed:iconName];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicator.frame = CGRectMake(160 - 25, 200 - 25, 50, 50);
        [self.view addSubview:self.activityIndicator];
        
        _loaded = NO;
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!_loaded)
    {    
        if (FBSession.activeSession.isOpen)
        {            
            // Get the FB credential stuff
            //NSString* fb_access_token = FBSession.activeSession.accessToken;
            
            // append to our target string
            // NSString* url_credential = [NSString stringWithFormat:@"%@?fb_user=%@&fb_oauth_secret=%@", self.targetUrl, fb_user, fb_oauth];
            NSString* url_credential = self.targetUrl;
            
            // fire off the webview request
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:url_credential]];
            [self.webView loadRequest:requestObj];

            NSLog(@"Loading: %@", url_credential);
        }
        else
        {
            [self.webView loadHTMLString:@"<center>Session not open?<center>" baseURL:nil];
        }
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
}

@end
