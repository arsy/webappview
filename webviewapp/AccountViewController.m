//
//  AccountControllerViewController.m
//  webviewapp
//
//  Created by Arsy on 11/6/12.
//  Copyright (c) 2012 Arsy. All rights reserved.
//

#import "AccountViewController.h"

@interface AccountViewController ()

@end

@implementation AccountViewController


- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    if (self)
    {
        self.title = @"Account";
        self.tabBarItem.image = [UIImage imageNamed:@"female_tab.png"];
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
